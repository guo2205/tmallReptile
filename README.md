# 使用说明 #

## 安装
1. 首先打开安装node-v9.3.0-x64.msi
2. 以管理员身份在当前目录打开命令行
3. 依次运行以下命令
+ npm install -g cnpm --registry=https://registry.npm.taobao.org
+ cnpm install


## 启动
1. 在当前目录打开命令行
2. 运行以下命令
+ npm start
3. 打开游览器前往http://localhost:8080，并关闭。


## 运行时
1. 程序会打印出运行中相关的log信息
2. 运行完成后会在save文件夹中生成一个.csv格式的表格记录

## 自定义配置
1. 在routes/index.js文件中data1,data2数组可以分别配置需要搜索的货号以及需要搜索的店铺